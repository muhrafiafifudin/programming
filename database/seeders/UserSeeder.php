<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $password = 'password';

        $super_admin = User::create([
            'name' => 'Super Admin',
            'email' => 'superadmin@gmail.com',
            'password' => $password,
            'created_at' => Carbon::now()
        ]);

        $super_admin->assignRole('Super Admin');
        $super_admin->givePermissionTo(
            'View Client',
            'Add Client',
            'Edit Client',
            'Delete Client',
            'View Order',
            'Add Order',
            'Edit Order',
            'Delete Order'
        );

        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => $password,
            'created_at' => Carbon::now()
        ]);

        $admin->assignRole('Admin');
        $admin->givePermissionTo(
            'View Client',
            'Add Client',
            'Edit Client',
            'Delete Client',
            'View Order',
            'Add Order',
            'Edit Order',
            'Delete Order'
        );

        $user = User::create([
            'name' => 'User',
            'email' => 'user@gmail.com',
            'password' => $password,
            'created_at' => Carbon::now()
        ]);

        $user->assignRole('User');
        $user->givePermissionTo(
            'View Client',
            'View Order'
        );
    }
}
