<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $clients = [
            [
                'name' => 'Anisa',
                'address' => 'Jakarta',
                'start_contract' => '2023-01-01',
                'end_contract' => '2023-01-02'
            ],
            [
                'name' => 'J Kovic',
                'address' => 'Solo',
                'start_contract' => '2023-01-01',
                'end_contract' => '2023-01-05'
            ],
            [
                'name' => 'Bowo',
                'address' => 'Semarang',
                'start_contract' => '2023-01-01',
                'end_contract' => '2023-01-10'
            ],
        ];

        array_map(function ($client): void {
            Client::create($client);
        }, $clients);
    }
}
