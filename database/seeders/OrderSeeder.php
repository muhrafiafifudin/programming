<?php

namespace Database\Seeders;

use App\Models\Order;
use App\Models\Client;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $client = Client::get();

        $orders = [
            [
                'client_id' => $client->where('name', 'Anisa')->first()->id,
                'item' => 'Kursi',
                'price' => 80000,
                'date' => '2023-01-01'
            ],
            [
                'client_id' => $client->where('name', 'Anisa')->first()->id,
                'item' => 'Meja',
                'price' => 150000,
                'date' => '2023-10-05'
            ],
            [
                'client_id' => $client->where('name', 'Bowo')->first()->id,
                'item' => 'Pintu',
                'price' => 200000,
                'date' => '2023-04-13'
            ],
        ];

        array_map(function ($order): void {
            Order::create($order);
        }, $orders);
    }
}
