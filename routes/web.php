<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    if (Auth::user()) {
        return redirect()->route('dashboard');
    }
    return view('pages.auth.login');
});

Route::group(['middleware' => 'auth'], function () {
    // Dashboard
    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard');
    // User
    Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
        Route::get('/', 'App\Http\Controllers\Main\UserController@index')->name('index');
        Route::post('/', 'App\Http\Controllers\Main\UserController@store')->name('store');
        Route::match(['put', 'patch'], '/{karyawan}', 'App\Http\Controllers\Main\UserController@update')->name('update');
        Route::delete('/{karyawan}', 'App\Http\Controllers\Main\UserController@destroy')->name('destroy');
    });
    // Client
    Route::group(['prefix' => 'client', 'as' => 'client.'], function () {
        Route::get('/', 'App\Http\Controllers\Menu\ClientController@index')->name('index');
        Route::post('/', 'App\Http\Controllers\Menu\ClientController@store')->name('store');
        Route::match(['put', 'patch'], '/{client}', 'App\Http\Controllers\Menu\ClientController@update')->name('update');
        Route::delete('/{client}', 'App\Http\Controllers\Menu\ClientController@destroy')->name('destroy');
    });
    // Order
    Route::group(['prefix' => 'order', 'as' => 'order.'], function () {
        Route::get('/', 'App\Http\Controllers\Menu\OrderController@index')->name('index');
        Route::post('/', 'App\Http\Controllers\Menu\OrderController@store')->name('store');
        Route::match(['put', 'patch'], '/{order}', 'App\Http\Controllers\Menu\OrderController@update')->name('update');
        Route::delete('/{order}', 'App\Http\Controllers\Menu\OrderController@destroy')->name('destroy');
        Route::get('/print-pdf', 'App\Http\Controllers\Menu\OrderController@printPdf')->name('print-pdf');
    });
});

require __DIR__ . '/auth.php';
