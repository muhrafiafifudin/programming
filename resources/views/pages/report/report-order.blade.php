<!DOCTYPE html>
<html>
<head>
    <title>{{ $title }}</title>
    <style>
        body { font-family: Arial, sans-serif; }

        h1 { color: #333; }

        p { color: #666; }

        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 10px
        }
    </style>
</head>
<body>
    <h1>{{ $title }}</h1>

    <br>

    <table>
        <tr>
            <td><strong>No.</strong></td>
            <td><strong>Nama Klien</strong></td>
            <td><strong>Item</strong></td>
            <td><strong>Price</strong></td>
            <td><strong>Tanggal Pembelian</strong></td>
        </tr>

        @php $no=1; @endphp
        @foreach ($orders as $order)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $order->client->name }}</td>
                <td>{{ $order->item }}</td>
                <td>{{ $order->price }}</td>
                <td>{{ $order->date }}</td>
            </tr>
        @endforeach
    </table>
</body>
</html>
