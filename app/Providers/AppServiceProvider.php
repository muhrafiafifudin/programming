<?php

namespace App\Providers;

use App\Interfaces\Main\UserInterface;
use App\Interfaces\Menu\OrderInterface;
use Illuminate\Support\ServiceProvider;
use App\Interfaces\Menu\ClientInterface;
use App\Repositories\Main\UserRepository;
use App\Repositories\Menu\OrderRepository;
use App\Repositories\Menu\ClientRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(UserInterface::class, UserRepository::class);
        $this->app->bind(ClientInterface::class, ClientRepository::class);
        $this->app->bind(OrderInterface::class, OrderRepository::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
