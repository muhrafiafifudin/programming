<?php

namespace App\Http\Controllers\Menu;

use PDF;
use App\Http\Controllers\Controller;
use App\Interfaces\Menu\OrderInterface;
use App\Http\Requests\Menu\OrderRequest;
use App\Models\Order;

class OrderController extends Controller
{
    private $orderRepository;

    /**
     * Initial __construct
     *
     * @param OrderInterface $orderRepository [explicite description]
     *
     * @return void
     */
    public function __construct(OrderInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a listing of the order resource
     *
     * @return void
     */
    public function index()
    {
        $orders = $this->orderRepository->allOrders();

        $clients = $this->orderRepository->allClients();

        return view('pages.menu.order', compact('orders', 'clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(OrderRequest $request)
    {
        $data = $request->validated();

        $this->orderRepository->storeOrder($data);

        return redirect()->route('order.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @return void
     */
    public function update(OrderRequest $request, string $id)
    {
        $data = $request->validated();

        $this->orderRepository->updateOrder($data, $id);

        return redirect()->route('order.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return void
     */
    public function destroy(string $id)
    {
        $this->orderRepository->destroyOrder($id);

        return redirect()->route('order.index');
    }

    public function printPdf()
    {
        $data = [
            'title' => 'Semua Data Order',
            'orders' => Order::get()
        ];

        $pdf = PDF::loadView('pages.report.report-order', $data);

        return $pdf->download('Data Order.pdf');
    }
}
