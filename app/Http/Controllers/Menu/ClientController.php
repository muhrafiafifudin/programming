<?php

namespace App\Http\Controllers\Menu;

use App\Http\Controllers\Controller;
use App\Http\Requests\Menu\ClientRequest;
use App\Interfaces\Menu\ClientInterface;

class ClientController extends Controller
{
    private $clientRepository;

    /**
     * Initial __construct
     *
     * @param ClientInterface $clientRepository [explicite description]
     *
     * @return void
     */
    public function __construct(ClientInterface $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * Display a listing of the client resource
     *
     * @return void
     */
    public function index()
    {
        $clients = $this->clientRepository->allClients();

        return view('pages.menu.client', compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(ClientRequest $request)
    {
        $data = $request->validated();

        $this->clientRepository->storeClient($data);

        return redirect()->route('client.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @return void
     */
    public function update(ClientRequest $request, string $id)
    {
        $data = $request->validated();

        $this->clientRepository->updateClient($data, $id);

        return redirect()->route('client.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return void
     */
    public function destroy(string $id)
    {
        $this->clientRepository->destroyClient($id);

        return redirect()->route('client.index');
    }
}
