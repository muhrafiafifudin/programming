<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Http\Requests\Main\UserRequest;
use App\Interfaces\Main\UserInterface;

class UserController extends Controller
{
    private $userRepository;

    /**
     * Initial __construct
     *
     * @param UserInterface $userRepository [explicite description]
     *
     * @return void
     */
    public function __construct(UserInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the user resource
     *
     * @return void
     */
    public function index()
    {
        $users = $this->userRepository->allUsers();

        return view('pages.main.user', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(UserRequest $request)
    {
        $data = $request->validated();

        $this->userRepository->storeUser($data);

        return redirect()->route('user.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @return void
     */
    public function update(UserRequest $request, string $id)
    {
        $data = $request->validated();

        $this->userRepository->updateUser($data, $id);

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return void
     */
    public function destroy(string $id)
    {
        $this->userRepository->destroyUser($id);

        return redirect()->route('user.index');
    }
}
