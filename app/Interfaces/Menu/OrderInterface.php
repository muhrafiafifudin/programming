<?php

namespace App\Interfaces\Menu;

interface OrderInterface
{
    public function allOrders();
    public function allClients();
    public function storeOrder($data);
    public function updateOrder($data, $id);
    public function destroyOrder($id);
}
