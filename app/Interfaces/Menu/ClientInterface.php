<?php

namespace App\Interfaces\Menu;

interface ClientInterface
{
    public function allClients();
    public function storeClient($data);
    public function updateClient($data, $id);
    public function destroyClient($id);
}
