<?php

namespace App\Interfaces\Main;

interface UserInterface
{
    public function allUsers();
    public function storeUser($data);
    public function updateUser($data, $id);
    public function destroyUser($id);
}
