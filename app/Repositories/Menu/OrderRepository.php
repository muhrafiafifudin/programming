<?php

namespace App\Repositories\Menu;

use App\Models\Order;
use App\Interfaces\Menu\OrderInterface;
use App\Models\Client;

class OrderRepository implements OrderInterface
{
    /**
     * Get all data order
     *
     * @return void
     */
    public function allOrders()
    {
        return Order::all();
    }

    /**
     * Get all data client
     *
     * @return void
     */
    public function allClients()
    {
        return Client::all();
    }

    /**
     * Store order to database
     *
     * @param $data $data [explicite description]
     *
     * @return void
     */
    public function storeOrder($data)
    {
        return Order::create($data);
    }

    /**
     * Update order to database
     *
     * @param $data $data [explicite description]
     * @param $id $id [explicite description]
     *
     * @return void
     */
    public function updateOrder($data, $id)
    {
        return Order::findOrFail($id)->update($data);
    }

    /**
     * Destroy order
     *
     * @param $id $id [explicite description]
     *
     * @return void
     */
    public function destroyOrder($id)
    {
        Order::destroy($id);
    }
}
