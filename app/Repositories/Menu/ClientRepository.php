<?php

namespace App\Repositories\Menu;

use App\Models\Client;
use App\Interfaces\Menu\ClientInterface;

class ClientRepository implements ClientInterface
{
    /**
     * Get all data client
     *
     * @return void
     */
    public function allClients()
    {
        return Client::all();
    }

    /**
     * Store client to database
     *
     * @param $data $data [explicite description]
     *
     * @return void
     */
    public function storeClient($data)
    {
        return Client::create($data);
    }

    /**
     * Update client to database
     *
     * @param $data $data [explicite description]
     * @param $id $id [explicite description]
     *
     * @return void
     */
    public function updateClient($data, $id)
    {
        return Client::findOrFail($id)->update($data);
    }

    /**
     * Destroy client
     *
     * @param $id $id [explicite description]
     *
     * @return void
     */
    public function destroyClient($id)
    {
        Client::destroy($id);
    }
}
