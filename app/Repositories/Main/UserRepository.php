<?php

namespace App\Repositories\Main;

use App\Models\User;
use App\Interfaces\Main\UserInterface;

class UserRepository implements UserInterface
{
    /**
     * Get all data user
     *
     * @return void
     */
    public function allUsers()
    {
        return User::all();
    }

    /**
     * Store user to database
     *
     * @param $data $data [explicite description]
     *
     * @return void
     */
    public function storeUser($data)
    {
        return User::create($data);
    }

    /**
     * Update user to database
     *
     * @param $data $data [explicite description]
     * @param $id $id [explicite description]
     *
     * @return void
     */
    public function updateUser($data, $id)
    {
        return User::findOrFail($id)->update($data);
    }

    /**
     * Destroy user
     *
     * @param $id $id [explicite description]
     *
     * @return void
     */
    public function destroyUser($id)
    {
        User::destroy($id);
    }
}
